title:	An update is available!
body:	Nah.sh changed their API, which makes it necessary for you to update Öffi.
button-positive:	Google Play | market://details?id=de.schildbach.oeffi
button-neutral:		Direct download | https://oeffi.schildbach.de/download.html
button-negative:	dismiss
